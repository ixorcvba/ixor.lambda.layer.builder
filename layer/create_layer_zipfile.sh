#!/bin/bash

set -e

[[ -z ${LAYER_ZIP_NAME} ]] && { echo "ERROR: LAYER_ZIP_NAME envvar should be set, exiting ..."; exit 1; }

[[ -f "${LAYER_ZIP_NAME}" ]] && rm -f ${LAYER_ZIP_NAME}

echo "Installing packages"
cd ..
sam build -u

ZIP_PREFIX=$(date '+%s')

echo "Bundling packages listed in requirements.txt into zip file"
mkdir -p .aws-sam/build/layers
cp -R .aws-sam/build/Function/* .aws-sam/build/layers/
cd .aws-sam/build/layers/ 
du -sch . 
find . -type d -name "tests" -exec rm -rf {} +
find . -type d -name "__pycache__" -exec rm -rf {} +
rm -rf ./{caffe2,wheel,pkg_resources,boto*,aws*,pip,pipenv,setuptools} 
rm -rf ./torch/test
rm -rf ./{*.egg-info,*.dist-info} 
find . -name \*.pyc -delete
rm -f {app.py,requirements.txt,__init__.py} 

# Do custom cleanup
if [[ -n ${LAYER_CUSTOM_CLEANUP} ]]; then
  # LAYER_CUSTOM_CLEANUP contains a space separates list of
  # directories to remove
  for d in ${LAYER_CUSTOM_CLEANUP}; do
    echo "Removing ${d} (custom cleanup)"
    rm -rf ${d}
  done
fi

du -sch . 
cd - 
# build the .requirements_${ZIP_PREFIX}.zip file
cd .aws-sam/build/layers/
zip -9 -q -r ../../../layer/.requirements_${ZIP_PREFIX}.zip . 
cd -

echo "Creating layer zipfile"
cd layer
zip -9 ${LAYER_ZIP_NAME} .requirements_${ZIP_PREFIX}.zip -r python/

echo "Delete the requirements zipfile & build directory"
rm .requirements_${ZIP_PREFIX}.zip
rm -rf ../.aws-sam/build
