import os
import shutil
import sys
import zipfile


pkgdir = '/tmp/sls-py-req'

sys.path.append(pkgdir)

default_layer_root = '/opt'
lambda_root = os.getcwd() if os.environ.get('IS_LOCAL') == 'true' else default_layer_root

print(f"lambda_root: {lambda_root}")

for zip_requirements in os.listdir(lambda_root):
  if zip_requirements.endswith(".zip"):
    print(f"Unzipping {zip_requirements}")
    zipfile.ZipFile(f"{lambda_root}/{zip_requirements}", 'r').extractall(pkgdir)

  print(os.listdir('/tmp/sls-py-req'))
