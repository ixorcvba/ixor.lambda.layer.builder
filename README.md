# ixor.lambda.layer.builder: Generic Lambda Layer builder

## Credentials

Thanks to [Matt McClean](https://github.com/mattmcclean) and
his [sam-pytorch-example](https://github.com/mattmcclean/sam-pytorch-example)
project to point us in the right direction and get _PyTorch_ to fit within
the AWS Lambda execution limitations.

## Description

This is a generic Lambda Builder. It can be used by repos, containing
only a `requirements.txt` file to build a Lambda Layer.

The provided `requirements.txt` file is used to build a zip file that can
be uploaded to AWS S3 and used as a AWS Lambda function Layer.

The Lambda Layer in turn contains a zip file, and that zipfile (inside the layer
zipfile) contains the Python modules.

The Lambda function itself (which is not in this repo because the function
has a lifecycle that is different from the dependency lifecycle) will have to
_import_ the `unzip_requirements` module. That module will unzip the
dependencies to `/tmp` inside the Lambda runtime environment.

Keep following Lambda limitations in mind:

* Total size of **all** used layers, including the function itself,
  should not exceed 250 MB
* The space in `/tmp` is limited to 512 MB. This means that the total
  size of all unzipped dependencies should not exceed 512 MB
* `/tmp` will remain unchanged if the function remains _warm_. Using
  `/tmp` to temporarily store files is not recommended, as the size
  might increase with every run if no or insufficient cleanup is
  performed. Use `S3` instead.
* Do not rely on files being present inside `/tmp` as these are not
  necessarily persistent

## Why put a zipfile in a zipfile?

This technique is used to circumvent the AWS Lambda 512 MB unzipped
layer limitation. The zipfile in the layer is unzipped in the `/tmp`
storage spaceavailalbe in the Lambda function, and the dependencies
will be loaded from that location.

## How to build the layer

### Prerequisites

* `aws-sam`
* Docker

### Prepare _your_ repository

* Create a repo with a `requirements.txt` file in the root
* Set and export the environment variable `LAYER_ZIP_NAME`. This
  could be part of the CI/CD pipeline in your repo
* Add the `build_layer.sh` script to the root of the
  same repository (do not change anything):

```bash
#! /bin/bash

git clone https://bitbucket.org/ixorcvba/ixor.lambda.layer.builder.git
cp requirements.txt ixor.lambda.layer.builder/function/requirements.txt
cd ixor.lambda.layer.builder/layer
./create_layer_zipfile.sh
```  

* The artifact, the zipfile containing the dependencies that can be
  used to create a Lambda layer, is
  `ixor.lambda.layer.builder/layer/${LAYER_ZIP_NAME}`
* `${LAYER_CUSTOM_CLEANUP}` contains a space separated list (no wildcards) of
  module directories to remove before creating the zip file
* Do something with the artifact, like uploading it to S3 where it can
  be found to create the Lambda layer.

## TODO: CI/CD

## How the Lambda function should look like

Layers only will not be enough, of course. The Lambda function will have to
take care of the unpacking of the zip file that is part of the layer you create
with this repo. The code snippet below shows the code that has to be in your
function file.

Some upfront remarks:

* All code outside the handler function is executed exaclt once, when
  the Lambda function goes from _cold_ to _warm_. All subsequent calls
  to the still warm function will only run de code inside the handler.

```python
# First of all, unzip the dependency zip file
try:
    import unzip_requirements
except ImportError:
    pass

import ...
```
